﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;
using System;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class UpdateAppliedPromocodeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public UpdateAppliedPromocodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                throw new Exception("В базе отсутствует сотрудник с указанным Id");

            employee.AppliedPromocodesCount++;
                        
            await _employeeRepository.UpdateAsync(employee);                         
        }
    }
}
