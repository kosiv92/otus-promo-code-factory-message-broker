﻿using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.CommonLibrary;
using System.Diagnostics;
using MassTransit;
using System;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class PromoCodeConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly UpdateAppliedPromocodeService _updateAppliedPromocodeService;

        public PromoCodeConsumer(UpdateAppliedPromocodeService updateAppliedPromocodeService)
        {
            _updateAppliedPromocodeService = updateAppliedPromocodeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            var request = context.Message;                        

            var managerId = request.PartnerManagerId ?? throw new Exception("Отсутствует id сотрудника");
                        
            await _updateAppliedPromocodeService.UpdateAppliedPromocodesAsync(managerId);
        }
    }
}
